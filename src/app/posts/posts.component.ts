import { Observable } from 'rxjs';
import { Posts } from './../interfaces/posts';
import { Component, OnInit } from '@angular/core';
import { PostsService } from './../posts.service';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts$:Observable<Posts>
  constructor(private postsService:PostsService) { }

  ngOnInit(): void {

    this.posts$ = this.postsService.getPosts();
  }

}