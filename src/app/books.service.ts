import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable, observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore'
import { title } from 'process';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  
  books = [{title:'Alice in Wonderland', author:'Lewis Carrol',summary:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book"},
  {title:'War and Peace', author:'Leo Tolstoy',summary:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout"},
  {title:'The Magic Mountain', author:'Thomas Mann',summary:"Ma  ny desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy"}];

  public addBooks(){
    setInterval(()=>this.books.push({title:'A new one', author:'New author', summary:'Short summary'}),2000);
  }

  /*
  public getBooks(){
    const booksObservable = new Observable(obs => {
      setInterval(()=>obs.next(this.books),500)
    }); 
    return booksObservable; 
  }

  */

 bookCollection:AngularFirestoreCollection;
 userCollection: AngularFirestoreCollection = this.db.collection(`users`);

  public getBooks(userId,startAfter){ 
    this.bookCollection = this.db.collection(`users/${userId}/books`,
    ref => ref.orderBy('title','asc').limit(4).startAfter(startAfter));
    return this.bookCollection.snapshotChanges() 
  }

/*
public getBooks(userId){
    this.bookCollection = this.db.collection(`users/${userId}/books`);
    return this.bookCollection.snapshotChanges().pipe(map(
      collection => collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data
        }
      )   
    ))
  }
  */

  deleteBook(userId:string, id:string){
    this.db.doc(`users/${userId}/books/${id}`).delete();

  }

  addBook(userId:string, title:string, author:string){
    const book = {title:title, author:author};
    this.userCollection.doc(userId).collection('books').add(book);
  }

  updateBook(userId:string, id:string,title:string,author:string){
    this.db.doc(`users/${userId}/books/${id}`).update(
      {
        title:title,
        author:author
      }
    )
  }

  constructor(private db:AngularFirestore) {}
}
