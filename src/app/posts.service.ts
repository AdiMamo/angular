import { Posts } from './interfaces/posts';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private URL = "https://jsonplaceholder.typicode.com/posts/";


  constructor(private http:HttpClient) {}
  getPosts():Observable<Posts>{
  return this.http.get<Posts>(this.URL);
  }
}
