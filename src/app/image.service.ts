import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private path:string = 'https://firebasestorage.googleapis.com/v0/b/hello-d5730.appspot.com/o/';
  public images:string[] = [];
  constructor() {
    this.images[0] = this.path + 'biz.png' + '?alt=media&token=58cde0bf-45b2-40f0-aaa0-247e256bacc5'
    this.images[1] = this.path + 'enterment.jfif' + '?alt=media&token=fbd80255-13ee-47c2-82a2-10882f3f8871';
    this.images[2] = this.path + 'politics.png' + '?alt=media&token=aced7127-e0a2-42ab-879a-b3cfd2028fe2';
    this.images[3] = this.path + 'sport.jfif' + '?alt=media&token=9ecb621e-0db3-4c3f-b929-dc34cbef0c01';
    this.images[4] = this.path + 'tech.png' + '?alt=media&token=717f9fca-5f39-4d9f-b101-34dd8fa2e7bd';
   }
}
